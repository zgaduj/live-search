import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LiveSearchModule} from './live-search/live-search.module';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LiveSearchModule // @todo: move to ng routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
