import {MediaType} from '../_const/media-types';

class MediaItemInterface {
  itemId?: number;
  type?: MediaType;
  title?: string;
  itemData?: any;
  createdAt?: Date;
}

export class MediaItemModel extends MediaItemInterface {
  constructor(data?: Partial<MediaItemInterface>) {
    super();
    Object.keys(data).forEach((k) => {
      this[k] = data[k];
    });
  }
}
