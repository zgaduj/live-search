export enum MediaType {
  Document,
  Image,
  Video,
  Audio,
}
