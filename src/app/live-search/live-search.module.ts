import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MediaItemComponent} from './components/media-item/media-item.component';
import {ItemsListComponent} from './components/items-list/items-list.component';
import {ItemsFilterComponent} from './components/items-filter/items-filter.component';
import {MediaItemsPageComponent} from './pagess/media-items-page/media-items-page.component';
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [MediaItemComponent, ItemsListComponent, ItemsFilterComponent, MediaItemsPageComponent],
  exports: [
    MediaItemsPageComponent
  ],
  imports: [
    CommonModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule
  ]
})
export class LiveSearchModule {
}
