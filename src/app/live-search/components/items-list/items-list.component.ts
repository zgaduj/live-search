import {Component, Input, OnInit} from '@angular/core';
import {MediaItemModel} from "../../models/media-item.model";

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {

  @Input() items: MediaItemModel[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
