import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {MediaType} from "../../_const/media-types";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ItemsOrder} from "../../services/fake-db.service";
import {Subscription} from "rxjs";

export interface MediaItemsFiltersFormInterface {
  type?: MediaType | false;
  keywords?: string;
  sort?: ItemsOrder;
  sortColumn?: string;
}

@Component({
  selector: 'app-items-filter',
  templateUrl: './items-filter.component.html',
  styleUrls: ['./items-filter.component.scss']
})
export class ItemsFilterComponent implements OnInit, OnDestroy {

  formFilters: FormGroup;
  private $formWatch: Subscription;

  filters = {
    types: [
      {
        value: MediaType.Document,
        label: 'Document'
      },
      {
        value: MediaType.Video,
        label: 'Video'
      },
      {
        value: MediaType.Audio,
        label: 'Audio'
      },
      {
        value: MediaType.Image,
        label: 'Image'
      },
    ],
    sortColumn: [
      {
        value: 'createdAt',
        label: 'Date Uploaded'
      },
      {
        value: 'title',
        label: 'Alphabetical'
      }
    ]
  };

  ItemsOrder = ItemsOrder;

  @Output() filtersChanged = new EventEmitter<MediaItemsFiltersFormInterface>();

  constructor(private fb: FormBuilder) {
    this.formFilters = fb.group({
      type: [false],
      keywords: [''],
      sort: [ItemsOrder.ASC],
      sortColumn: [this.filters.sortColumn[0].value]
    });
  }

  ngOnInit(): void {
    this.$formWatch = this.formFilters.valueChanges.subscribe((changes) => {
      this.filtersChanged.next(changes);
    });
    this.resetFilters(); // set default values
  }

  ngOnDestroy(): void {
    if (this.$formWatch) {
      this.$formWatch.unsubscribe();
    }
  }

  resetFilters(): void {
    const ifaceForm: MediaItemsFiltersFormInterface = {
      keywords: '',
      sort: ItemsOrder.DESC,
      sortColumn: 'createdAt',
      type: false
    }
    this.formFilters.reset(ifaceForm);
  }

  switchSorting(): void {
    const sortInput = this.formFilters.get('sort');
    const actual = sortInput.value as ItemsOrder;
    sortInput.patchValue(actual === ItemsOrder.DESC ? ItemsOrder.ASC : ItemsOrder.DESC);

  }
}
