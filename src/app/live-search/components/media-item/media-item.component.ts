import {Component, Input, OnInit} from '@angular/core';
import {MediaItemModel} from "../../models/media-item.model";

@Component({
  selector: 'app-media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.scss']
})
export class MediaItemComponent implements OnInit {

  @Input() itemData: MediaItemModel;

  constructor() {
  }

  ngOnInit(): void {
  }

}
