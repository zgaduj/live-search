import {Component, OnInit} from '@angular/core';
import {FakeDbService, ItemsOrder} from "../../services/fake-db.service";
import {MediaItemsFiltersFormInterface} from "../../components/items-filter/items-filter.component";
import {MediaItemModel} from "../../models/media-item.model";

@Component({
  selector: 'app-media-items-page',
  templateUrl: './media-items-page.component.html',
  styleUrls: ['./media-items-page.component.scss']
})
export class MediaItemsPageComponent implements OnInit {
  itemsList: MediaItemModel[] = [];

  constructor(private fakeDbService: FakeDbService) {
  }

  ngOnInit(): void {
  }

  filtersChanged(changes?: MediaItemsFiltersFormInterface): void {
    this.fakeDbService.find(changes && {
      title: changes.keywords,
      type: changes.type
    },
      {
        by: changes && changes.sortColumn || 'createdAt',
        sort: ItemsOrder[changes && changes.sort] ? changes.sort : ItemsOrder.DESC
      }
    )
      .subscribe((data) => {
        this.itemsList = data;
      }).unsubscribe();
  }
}
