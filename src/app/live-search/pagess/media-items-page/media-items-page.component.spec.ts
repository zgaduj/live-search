import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MediaItemsPageComponent} from './media-items-page.component';

describe('MediaItemsPageComponent', () => {
  let component: MediaItemsPageComponent;
  let fixture: ComponentFixture<MediaItemsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MediaItemsPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaItemsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
