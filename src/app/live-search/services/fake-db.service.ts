import {Injectable} from '@angular/core';
import {MediaItemModel} from "../models/media-item.model";
import {MediaType} from "../_const/media-types";
import {of} from "rxjs";
import {map} from "rxjs/operators";
import {fakeDB} from "./fake-db";

export enum ItemsOrder {
  ASC,
  DESC
}

@Injectable({
  providedIn: 'root'
})
export class FakeDbService {

  items = of<MediaItemModel[]>(fakeDB);

  constructor() {
  }

  find(where?: { title?: string, type?: MediaType | false }, sort?: { by: string, sort: ItemsOrder }) { // fake REST API
    return this.items.pipe(
      map((items) => {
        return items.filter((item) => {
          const pass: boolean[] = [];
          if (where && where.type !== false) {
            pass.push(item.type === where.type);
          }

          if (where && where.title) {
            pass.push(
              item.title.search(new RegExp(where.title, 'gmi')) >= 0
            );
          }

          if (!pass.length) {
            return true;
          }
          return pass.every(v => v === true);
        }).sort((a, b) => {
          if (sort === undefined) {
            return;
          }
          switch (sort.sort) {
            case ItemsOrder.ASC:
              // @ts-ignore
              return this._sortItems(a, b, sort.by);
            case ItemsOrder.DESC:
              // @ts-ignore
              return this._sortItems(b, a, sort.by);
          }
          return null;
        })
      }),
    );
  }

  private _sortItems(a: MediaItemModel, b: MediaItemModel, by: string) {
    if (typeof a[by] === 'string') {
      return a[by].localeCompare(b[by], undefined, {numeric: true, sensitivity: 'base'});
    }
    return a[by] - b[by];
  }
}
