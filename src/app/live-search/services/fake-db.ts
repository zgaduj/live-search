import {MediaItemModel} from "../models/media-item.model";
import {MediaType} from "../_const/media-types";

export const fakeDB: MediaItemModel[] = [];
const randomString = () => {
  return Math.random().toString(36).substring(7);
};
const randomDate = () => {
  const start = new Date(new Date().getTime() - (60 * 60 * 24 * 4 * 1000));
  const end = new Date();
  // @ts-ignore
  const date = new Date(+start + Math.random() * (end - start));
  const hour = Math.random();
  date.setHours(hour);
  return date;
};
let itemId = 1;
// generate videos
for (let i = 1; i <= 10; i++) {
  fakeDB.push( // there can use Object.assign(new MediaItemModel(), {data});
    new MediaItemModel(
      {
        itemId: itemId++,
        title: `#${i} video ${randomString()}`,
        createdAt: randomDate(),
        itemData: {},
        type: MediaType.Video
      }
    )
  );
}
// generate documents
for (let i = 1; i <= 10; i++) {
  fakeDB.push( // there can use Object.assign(new MediaItemModel(), {data});
    new MediaItemModel(
      {
        itemId: itemId++,
        title: `#${i} document ${randomString()}`,
        createdAt: randomDate(),
        itemData: {},
        type: MediaType.Document
      }
    )
  );
}
// generate images
for (let i = 1; i <= 10; i++) {
  fakeDB.push( // there can use Object.assign(new MediaItemModel(), {data});
    new MediaItemModel(
      {
        itemId: itemId++,
        title: `#${i} image ${randomString()}`,
        createdAt: randomDate(),
        itemData: {},
        type: MediaType.Image
      }
    )
  );
}
// generate audio
for (let i = 1; i <= 10; i++) {
  fakeDB.push( // there can use Object.assign(new MediaItemModel(), {data});
    new MediaItemModel(
      {
        itemId: itemId++,
        title: `#${i} audio ${randomString()}`,
        createdAt: randomDate(),
        itemData: {},
        type: MediaType.Audio
      }
    )
  );
}

// console.log('fakeDB', fakeDB)
