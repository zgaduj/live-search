FROM node:10 as build-stage
#ENV ENVIRONMENT
WORKDIR /var/project
COPY . /var/project

RUN npm install
RUN npm run build

FROM nginx:latest
COPY --from=build-stage /var/project/build /usr/share/nginx/html
RUN cd /usr/share/nginx/html && ls -al
COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
